
Pod::Spec.new do |specs|

  specs.name         = 'Cynage'
  specs.version      = '1.0.2'
  specs.license       = 'MIT'
  specs.homepage    = 'https://gitlab.com/arkapravaghosh99/cynage'
  specs.summary      = 'Cynage is a framework for utility.'
  specs.description  = 'Cynage framework is for utility purpose as of now. Will support more helper functions in future.'
  specs.license     = { :type => 'BSD' }
  specs.author       = { "Arkaprava Ghosh" => "arkapravaghosh99@gmail.com" }
  specs.source       = { :git => 'https://gitlab.com/arkapravaghosh99/cynage', :tag => '1.0.0' }
  specs.ios.deployment_target  = '15.0'
  specs.source_files  = 'Cynage/**/*.{swift}'
  specs.swift_version  = '5.8'

end
